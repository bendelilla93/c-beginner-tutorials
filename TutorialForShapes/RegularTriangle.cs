﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TutorialForShapes
{
    public class RegularTriangle : Triangle, IRegular
    {
        //Sides of the triangle-double because of Perimeter()
        private double a;
        public ulong NumberOfSides
        {
            get;
            private set;
        }

        public double SizeOfOneSide
        {
            get { return a; }
        }
       

        //The constructor of Triangle class
        public RegularTriangle(int id, string name, double a): base(id, name, a, a, a)
        {
            this.a = a;
            this.NumberOfSides = 3;
           
        }

        //Perimeter() method according to IPolygon
        double IRegular.Perimeter()
        {
            return base.Perimeter();
        }

        double IRegular.Area()
        {
            double x = 0.25 * a * a * 3;

            return x * System.Math.Tan(System.Math.PI / 3);
               
        }

    }
}
