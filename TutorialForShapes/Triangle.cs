﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForShapes
{
    public class Triangle : IPolygon
    {
        //Sides of the triangle-double because of Perimeter()
        private double a;
        private double b;
        private double c;

        // Every triangle has its own Id and Name property-private set because it is not part of IShape 
        public int Id
        {
            get;
            private set;
        }
        

        public string Name
        {
            get;
            private set;
        }

        //The constructor of Triangle class
        public Triangle(int id, string name, double a, double b, double c)
        {
            this.Id = id;
            this.Name = name;
            this.a = a;
            this.b = b;
            this.c = c;
        }

        //Perimeter() method according to IPolygon
        public double Perimeter()
        {
            return a + b + c;
        }

       
    }
}
