﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForShapes
{
    public interface IShape
    {
        int Id { get; }
        string Name { get; }

    }
}
