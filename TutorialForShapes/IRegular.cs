﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TutorialForShapes
{
    public interface IRegular
    {
        ulong NumberOfSides { get; }
        double SizeOfOneSide { get; }

        double Perimeter();
        double Area();

    }
}
